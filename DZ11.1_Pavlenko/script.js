'use strict';
let modal = document.getElementById("modal_1");
let btn = document.getElementById("btn_1");
let span = document.getElementsByClassName("modal__close")[0];

btn.onclick = function() {
  modal.style.display = "block";
}

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target === modal) {
    modal.style.display = "none";
  }
} 